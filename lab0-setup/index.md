# Lab 0: Setup & Getting Started


Logging into all the Red Hat Products:

Let’s log into the Red Hat Products that you will use in this lab so they are ready to use.

In this lab application based self-signed SSL certs are used, please note that they are being used and should be accepted in order to complete the lab exercises.

1. Open Firefox to The Red Hat Summit [GUID Grabber](https://www.opentlc.com/gg/gg.cgi?profile=generic_tester) application in order to obtain your lab GUID.
2. For Lab Code select <LAB> WKSP1 - Red Hat Satellite and Insights Test Drive
3. Enter the activation key provided by the lab instructor then click Next. For this lab the key is <KEY>  SAT007
4. In your Red Hat Summit Lab Information webpage, take note of your assigned GUID. You will use this GUID to access your lab’s systems. Click on the link at the bottom of this page to access your lab environment. You should *not* close this tab or this page, its handy to have available as you proceed through the hands on workshop.

![](images/image55.png) 

5.  From the lab environment information page, copy the hostname of the
    Workstation system (it should be workstation-GUID.rhpds.opentlc.com
    where GUID matches your environment’s GUID).

6.  Since this is a hands on lab we have built a predefined key and set that up for the 'root' user to simplify logins. We will use the generally frowned upon root user to access these systems since we're working in a time constraint. Its never a good idea to use root outside of a sandboxed non-production lab environment. Usually the root account itself is disabled for non local logins. Definitely don't use root on any production systems. 'sudo' exists for a reason. You've been warned.

7.  Download the ["summit_lab_rsa" private key](https://gitlab.com/tronik/2019SatTestDrive/raw/46d5cef94e126e851cc46902d7e0ece86e73b9d3/summit_lab_rsa) and store that key in your desktops .ssh directory or in your private key file store. (If using Linux or a Mac you will need to make sure the private key has the appropriate permissions. You can use 'chmod go-rwx summit_lab_rsa'). The private key MAY download as a .txt file - you can leave the private key with the .txt and amend commands below or rename it to 'summit_lab_rsa' to use it. Refer to step 9 for the actual parameters.

8.  Open a terminal window/ssh client on your desktop environment and make sure you can SSH into the workstation host as you see below in steps 9 and 10.

9.  [username@MY-BYOD-SYSTEM \~]\$ ssh -i summit_lab_rsa root@workstation-GUID.rhpds.opentlc.com

    If your SSH client is in Windows or a GUI please make sure to use the 'root' user and specify the private key as mentioned in step 7. Definitely don't use root on any production systems. 'sudo' exists for a reason. You've been warned again. Additionally "GUID" in the above syntax references the GUID you should know from Step 1 or Step 4 of the previous section.

    [root@workstation-f902 ~]# 

From the above prompt you are now on a jumpbox workstation in your virtual environment. Here you will perform all the administrative tasks in the lab.

10.  When ssh'ing at any time if you're asked if you're sure you wish to connect just answer 'yes' to override any warnings or errors that you may receive due to the nature of the hands on exercise setup.

10a. The hosts in this lab you may ssh to from the jumpbox are 'ic1.example.com' 'ic2.example.com' 'ic3.example.com' ic4.example.com' and 'sat.example.com' - You access ALL of these hosts from the jumpbox workstation you connect to from step 9 above. You can login to each of those systems now, just pay attention to which systems you're running any required commands on as you proceed through the lab. Any need to reset the lab will severely impact the available lab time remaining to complete your tasks.

optional help: This step is not required, but If you need to troubleshoot or power
on/off/reboot a system, you can use the environment’s power control and
consoles by clicking the link on your GUID page. The password for any
consoles will be with username ‘root’ and password “redhat”. From this
page, you will be able to access all of the Red Hat Products that you
will use in this lab. Press the start button at the top right to turn on
all the Red Hat Product VMs. Then, click on https for all the Red Hat
Products to access the UI. For applications, you may also log into the
UI of all the Red Hat Products with ‘admin’ as the Username and
“redhat” (without the quotes) as the Password.
(The following picture is just an example, your VM names may be different depending on the virtual lab you're working through, but the start/stop and console functionality is the same.)

![](images/image40.png)

The following labs take place within the fictional EXAMPLE.COM company.

Continue to next step: [Lab 1: Red Hat Satellite for Content Lifecycle Management](../lab1-satellite-lifecycle/index.md)