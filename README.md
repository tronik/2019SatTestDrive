# Summit 2019 Satellite and Insights Test Drive

---

The goal of these lab exercises are to introduce you to Red Hat Satellite and Red Hat Insights. Management software that can help you with managing and automating infrastructure resources.

Upon completion of this lab material, you should have a better understanding of how to perform  management tasks in your environments, how to more securely maintain hosts, and how to achieve greater efficiencies with automating tasks and managing systems within your infrastructure.

This lab is geared towards systems administrators, architects, and others working on infrastructure operations interested in learning how to automate management across a heterogeneous infrastructure.
The prerequisite for this lab include basic Linux skills gained from Red Hat Certified System Administrator (RHCSA) or equivalent system administration skills, but if you perform lab guides as documented you should have plenty of time to complete the lab given limited or no experience with the software previously.

What You Will Need: An SSH Client (like ssh, putty (windows), kitty(windows), or WinSCP with the ability to install a PRIVATE KEY FILE to access a jumpbox in this lab. A modern Web Browser (Chrome or Firefox) to interact with Satellite 6. The private key file is in THIS GIT REPO as summit_lab_rsa

Knowledge of virtualization and basic Linux scripting would also be helpful, but not required.

There are workshop assistants and presenters roaming the room to help you with issues or answer questions - just raise your hand!

## Table of Content

[Lab 0: Setup & Getting Started](lab0-setup/index.md)

[Lab 1: Red Hat Satellite for Content Lifecycle Management](lab1-satellite-lifecycle/index.md)

[Lab 2: Satellite for Content Host Management](lab2-satellite-content/index.md)

[Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](lab3-insights/index.md)
