# Lab 2: Satellite for Content Host Management

<!-- TOC -->

- [Lab 2: Satellite for Content Host Management](#lab-2-satellite-for-content-host-management)
	- [Create Activation Key:](#create-activation-key)
	- [Register Content Hosts](#register-content-hosts)
	- [Update Content Hosts:](#update-content-hosts)

<!-- /TOC -->

There are several preconfigured activation keys in the lab environment.
Some of these are required for subsequent lab functions. Do not remove these activation keys.

### Create Activation Key:

1.  Navigate to Activation Keys

	* To Navigate to the Activation Keys page, select “Content” from the left hand navigation bar, and select “Activation Keys” .

2.  Create Activation Key

	* From the Activation Keys page, click the blue “Create Activation Key” button in the top right corner
	* Name Activation Key whatever you’d like (we recommend something simple like "Yourname" as you will need to call this from the command line in an upcoming lab) and associate with your new Lifecycle Environment and Content View from the previous lab.

    **NOTE:** We have pre-built a Lifecycle Environment and Content View in case you did not do Lab 1. If that’s the case, feel free to use these pre-built objects going forward.

	* Click “Save” to save your Activation Key.

![](images/image28.jpg)

3.  Attach Subscriptions to Activation Key

	*  From the Activation Keys page, select your newly created activation key.
	*  Select “Subscriptions” from the Activation Key toolbar

	**NOTE:** If continuing from the previous step, this may direct you to this page automatically.

	* Select “Add” from the new section available.
	* Click the checkbox next to the “Red Hat Enterprise Linux Server, Premium (Physical or Virtual Nodes)” and then click “Add Selected” in the top right corner of the this new section.  

    **NOTE:** There may be multiple subscriptions of this type listed.  Select the first one.

![](images/image100.jpg)

4.  Configure “Enabled Repository” Default

	* From the Activation Keys page, select your newly created Activation Key
	* Select “Repository Sets” from the Activation Key toolbar

	**NOTE:** If continuing from the last step, this may direct you to this page automatically.

	**NOTE:** the repositories may take a moment to load due to using the Employee SKU in this example.

	* Click the checkbox next to each of the following repositories

	```
	Red Hat Enterprise Linux 7 Server (Kickstart)
	Red Hat Enterprise Linux 7 Server (RPMs)
    Red Hat Enterprise Linux 7 Server - Extras (RPMs)
	Red Hat Satellite Tools 6.4 (for RHEL 7 Server) (RPMs)
	```

	* Click “Select Action” followed by “Override to Enabled” in the top right corner of the Repository Selection section.

![](images/image7.jpg)

**Background:**

By creating an Activation Key, and associating the created Lifecycle Environments, Content Views, Subscriptions, and Repositories, we’ve enable the ability to activate simple, precise, and efficient deployment of new systems.

Activation Keys can be used to easily automate provisioning, as well as allow for consistent registration and initialization of Content Hosts without requiring users to obtain admin credentials.

### Register Content Hosts

1.  Navigate to Content Hosts

	* To Navigate to the Content Hosts page, select “Hosts” from your toolbar, and select “Content Hosts” from the dropdown.
	
		**Note:** You should only see your satellite profile listed here. As we continue through registering other client systems, you will begin to see this page populated.

![](images/image41.jpg)

2.  Register Content Host

	* Click “Register Content Host” button in the top right corner
	* Follow steps outlined in the Satellite WebUI. Leave sat.example.com as the content source. Perform the commands on system ic1.example.com. Login to the workstation jumpbox as [outlined in Lab 0](https://gitlab.com/tronik/2019SatTestDrive/blob/3872bff1d175b1c4f39b9fa1749bc9e32cfa6a61/lab0-setup/index.md) and then ssh to ic1.example.com to run the commands on the Content Host Registration page.

		1. * Select Content Source (this will be your desired Satellite or Capsule server). Based on your selection here, it will adjust the following rpm location accordingly. For the scope of this lab, this will simply be your satellite, sat.example.com.
		2. * Install pre-built bootstrap RPM. This adjusts configurations on the host to point to your satellite/capsule for registration, subscription, and content delivery.
		3. * From the client console, run the listed subscription-manager command, using the Activation Key you created in the previous section.
		* To retrieve a list of Activation keys navigate to the Activation Keys page, select “Content” from the left hand navigation bar, and select “Activation Keys”. You named the activation key in a previous section, if you used "Yourname" this should be easy!
		* If this fails with a note about already being registered, feel free to use the --force option here.
		* Ensure Satellite Tools repository is enabled (If Activate Key was pre-configured to enable this repo by default, this should be set in the last step). This provides access to Katello Agent in the next step.
		4. * Install Katello Agent, which provides the ability to run remote execution(like content patching), as well as displays the errata status (applicable bugs, security, etc) for each Content Host.

	* Repeat for client systems ic[2-4].example.com

If you didn't already open terminals to those machines from your jumpbox, remember to use the summit_lab_rsa identity private key from Lab 0 in order to connect. From the jumpbox then you can connect to any of the systems in this lab.
	
[username@MY-BYOD-SYSTEM \~]\$ ssh -i summit_lab_rsa root@workstation-GUID.rhpds.opentlc.com	

[root@workstation-GUID] # ssh ic1.example.com

Once you complete these steps every client in this virtual lab will be registered to this satellite server. 

![](images/image99.jpg)

        


### Update Content Hosts:

1.  Navigate to Content Host

	* To Navigate to the Content Hosts page, select “Hosts” from your toolbar, and select “Content Hosts” from the dropdown.
	* Notice after registering with the previously configured activation key, we now have the newly registered Content Host reporting it’s subscription status, Lifecycle Environment and Content View associations, and Installable Errata.
	* Some of the hosts may have the .localdomain domain instead of .example.com domain. This is a limitation of the lab     environment’s networking, but all actions should complete successfully against these hosts in the environments.

![](images/image1.jpg)

2.  Navigate to Content Host’s Installable Errata
	
	* Make note that ic1.example.com should be showing no errata needing to be installed! This system is (likely) completely up to date -- this may vary depending on the day and time you are doing this particular lab exercise. Compare ic1.example.com to ic2.example.com.

	* From the Content Hosts page, select a Content Host, for example ic2.example.com

	![](images/image96.jpg)

	* This will bring up the Details page by default. From here, select “Errata” from the Content Hosts toolbar.

	![](images/image54.png)

3.  Update Content Host

	* Looking at the Errata for this system, it’s clear the system is severely out of date. Given this, we have a couple of options to update the clients (Specifically Push/Pull).

		* Pull: Probably the most widely recognized method, and sometimes faster than selecting hundreds of errata from Satellite UI, from the client system, we can simply run “yum update” from the client console after registering and subscribing:

			* Login as root in the workstation, and SSH to the host for this exercise, in this case

    		```
			[username@MY-BYOD-SYSTEM ~]$ ssh -i summit_lab_rsa root@workstation-GUID.rhpds.opentlc.com    		
			[root@workstation-repl ~]# ssh ic2.example.com
			[root@ic2 ~]# yum -y update
			```

		* Push: We can push specific errata from the satellite, to the client by selecting the specific desired errata from this list, and clicking “Apply Selected”.

	* To Install All Errata you can select to view up to 100 available errata at a time at the bottom of the screen, and select all from the master checkbox in the top left corner of the view, then Apply Selected.

	* Repeat this step until all errata have been applied. [Due to limited time in the lab you may only want to install a few erratum]

**Note:** This is an FYI and does not need to be performed in this lab environment.

For systems registered to a prior Satellite, like this scenario: there may be some old remnants of the previous system registration. In this case, the remote push errata update may fail. You can login to the system and as root run the commands:

			# yum clean all ;  rm -rf /var/cache/yum/*

	* This clears out the old yum repo data from the previous satellite
    registration, and prepares the system for updates from your newly
    configured Satellite server.

Continue to the next step:[Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](../lab3-insights/index.md)
